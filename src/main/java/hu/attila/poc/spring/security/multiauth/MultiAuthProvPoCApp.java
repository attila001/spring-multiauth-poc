package hu.attila.poc.spring.security.multiauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiAuthProvPoCApp {

    public static void main(String[] args) {
        SpringApplication.run( MultiAuthProvPoCApp.class, args );
    }
}
