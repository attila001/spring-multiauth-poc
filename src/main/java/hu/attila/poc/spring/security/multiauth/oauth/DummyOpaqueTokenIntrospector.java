package hu.attila.poc.spring.security.multiauth.oauth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionException;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

public class DummyOpaqueTokenIntrospector implements OpaqueTokenIntrospector {

    @Override
    public OAuth2AuthenticatedPrincipal introspect( String token ) {
        if ( "04b4aeaa-10da-4870-8d4b-d0a2d42e3d13".equals( token ) ) {
            return new DefaultOAuth2AuthenticatedPrincipal( createDummyAttriburtes(), new ArrayList<>() );
        }
        throw new OAuth2IntrospectionException( "The token is not valid" );
    }

    private Map<String, Object> createDummyAttriburtes() {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put( "sub", "pista" );

        return attributes;
    }
}
