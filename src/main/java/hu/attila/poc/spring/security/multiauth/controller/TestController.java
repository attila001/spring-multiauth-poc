package hu.attila.poc.spring.security.multiauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/api/ping")
    public String getPing() {
        return "OK";
    }
}
