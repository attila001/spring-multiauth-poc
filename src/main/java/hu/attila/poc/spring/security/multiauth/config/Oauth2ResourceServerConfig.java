package hu.attila.poc.spring.security.multiauth.config;

import java.text.ParseException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationManagerResolver;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.OpaqueTokenAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import hu.attila.poc.spring.security.multiauth.oauth.DummyOpaqueTokenIntrospector;

@Configuration
@EnableWebSecurity
public class Oauth2ResourceServerConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure( HttpSecurity http ) throws Exception {

        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .oauth2ResourceServer()
                .authenticationManagerResolver(
                        tokenAuthenticationManagerResolver(
                                createJwtDecoder(),
                                new DummyOpaqueTokenIntrospector()
                        )
                );
    }

    private JwtDecoder createJwtDecoder() {
        NimbusJwtDecoder jwtDecoder = new NimbusJwtDecoder( new Oauth2ResourceServerConfig.ParseOnlyJWTProcessor() );
        jwtDecoder.setJwtValidator( token -> OAuth2TokenValidatorResult.success() );
        return jwtDecoder;
    }

    private AuthenticationManagerResolver<HttpServletRequest> tokenAuthenticationManagerResolver(
            JwtDecoder jwtDecoder,
            OpaqueTokenIntrospector opaqueTokenIntrospector
    ) {
        AuthenticationManager jwt = new ProviderManager( new JwtAuthenticationProvider( jwtDecoder ) );
        AuthenticationManager opaqueToken = new ProviderManager(
                new OpaqueTokenAuthenticationProvider( opaqueTokenIntrospector ) );

        return (request) -> useJwt( request ) ? jwt : opaqueToken;
    }

    private boolean useJwt( HttpServletRequest request ) {
        return !isUUID( request.getHeader( "authorization" ) );
    }

    private boolean isUUID( String token ) {
        try {
            UUID.fromString( token.replace( "Bearer ", "" ) );
            return true;
        } catch ( Exception e ) {
            return false;
        }
    }

    private static class ParseOnlyJWTProcessor extends DefaultJWTProcessor<SecurityContext> {
        @Override
        public JWTClaimsSet process( SignedJWT jwt, SecurityContext context ) throws JOSEException {
            try {
                return jwt.getJWTClaimsSet();
            } catch ( ParseException e ) {
                throw new JOSEException( e.getMessage(), e );
            }
        }
    }
}
